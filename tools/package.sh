#!/usr/bin/env bash

set -e

case $(uname -s) in
    Darwin) shasum="command shasum -a 256";;
    Linux) shasum="command sha256sum";;
    *)
        # Someday, support installing on Windows.  Service creation could be tricky.
        echo "No idea how to check sha256sums"
        exit 1;;
esac

export GO111MODULE=on

mkdir -p tools/build
exepath="$PWD/tools/build"
[[ -x $exepath/drpcli ]] || \
    go build -o "$exepath/drpcli" tools/drpcli.go
export PATH="$PWD/tools/build:$PATH"

. tools/version.sh
branch=${MajorV}.${MinorV}
. tools/name.sh

DOC_VERSION=""
if [[ "$CI_COMMIT_BRANCH" != "" ]] ; then
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%%-*}
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%.*}
  DOC_VERSION=$CI_COMMIT_BRANCH
fi
if [[ "$CI_COMMIT_TAG" != "" ]] ; then
  CI_COMMIT_TAG=${CI_COMMIT_TAG%%-*}
  CI_COMMIT_TAG=${CI_COMMIT_TAG%.*}
  DOC_VERSION=$CI_COMMIT_TAG
fi
if [[ "$DOC_VERSION" == "v4" ]] ; then
  DOC_VERSION=tip
fi
if [[ "$VERSION" == "" ]] ; then
  DOC_VERSION=tip
fi

echo -n "$version" > content/._Version.meta
(cd content && drpcli contents bundle ../$name.yaml)
$shasum $name.yaml > $name.sha256
echo "Publishing docs"
mkdir -p rebar-catalog/docs/$branch
drpcli contents document $name.yaml > rebar-catalog/docs/$branch/$name.rst
echo "Building documents $DOC_VERSION: $i"
drpcli contents document-md $name.yaml rackn-base-docs/$DOC_VERSION || :

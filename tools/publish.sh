#!/usr/bin/env bash

set -e

[[ $GOPATH ]] || export GOPATH="$HOME/go"
fgrep -q "$GOPATH/bin" <<< "$PATH" || export PATH="$PATH:$GOPATH/bin"

export GO111MODULE=on
go get -u github.com/stevenroose/remarshal
go install github.com/stevenroose/remarshal

. tools/name.sh
. tools/version.sh
echo "Publishing $i to cloud"
mkdir -p "rebar-catalog/$name"
remarshal -i $name.yaml -o $name.json -if yaml -of json
cp $name.json rebar-catalog/$name/$version.json
